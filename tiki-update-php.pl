#!/usr/bin/perl

=head1 tiki-update-php.pl

Check if the PHP version of the website and Tiki manager are in sync, if not update Tiki Manager.

=cut

if (!$module_name) {
	$main::no_acl_check++;
	$ENV{'WEBMIN_CONFIG'} ||= "/etc/webmin";
	$ENV{'WEBMIN_VAR'} ||= "/var/webmin";
	if ($0 =~ /^(.*)\/[^\/]+$/) {
		chdir($pwd = $1);
		}
	else {
		chop($pwd = `pwd`);
		}
	$0 = "$pwd/tiki-update-php.pl";
	require './virtualmin-tikimanager-lib.pl';
	$< == 0 || die "tiki-update-php.pl must be run as root";
	}
@OLDARGV = @ARGV;
&virtual_server::set_all_text_print();

# Parse command-line args
while(@ARGV > 0) {
	local $a = shift(@ARGV);
	if ($a eq "--domain") {
		$dname = shift(@ARGV);
		}
	elsif ($a eq "--help") {
		&usage();
		}
	else {
		&usage("Unknown parameter $a");
		}
	}

# Validate inputs
if ($dname) {
	$d = &virtual_server::get_domain_by("dom", $dname);
	$d || &usage("No virtual server named $dname found");
	}
else {
	&usage("--domain must be provided");
	}

if ($d) {
	$d->{'virtualmin-tikimanager'} ||
		&usage("Virtual server $d->{'dom'} does not have Tiki Manager active");
	}

my ($info) = &tikimanager_tiki_info($d);
if (! $info->{'instance_id'}) {
    &error("There is no instance linked to this domain.");
}

my $domain_php_exec = &tikimanager_get_domain_php_exec($d);
my $domain_php_ver = &get_php_version($domain_php_exec);

my $instance_php_ver = &get_php_version($info->{'phpexec'});

if ($domain_php_ver != $instance_php_ver) {
  &$virtual_server::first_print("Detected PHP CLI version from Tiki Manager does not match the one used by the website.");
  &$virtual_server::second_print("Reported by Tiki Manager: " . $info->{'phpexec'} . " (".$instance_php_ver.")");
  &$virtual_server::second_print("Reported by Virtualmin Website: " . $domain_php_exec . " (".$domain_php_ver.")");
  &$virtual_server::second_print("Updating to use " . $domain_php_exec . " ....");
  &tikimanager_execute_command("instance:edit --instances " . $info->{'instance_id'} . " --no-interaction --phpexec " . $domain_php_exec);
  &$virtual_server::second_print("... done");
}

sub usage
{
print "$_[0]\n\n" if ($_[0]);
print "Updates an instance PHP version to match the version of the domain in Virtualmin.\n";
print "\n";
print "virtualmin tiki-update-php --domain name\n";
exit(1);
}
