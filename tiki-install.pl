#!/usr/bin/perl
use strict;
use warnings;

use Cwd;
use File::Basename;
use Email::Valid;

our %in;
our $module_name;

$ENV{'WEBMIN_CONFIG'} ||= "/etc/webmin";
$ENV{'WEBMIN_VAR'} ||= "/var/webmin";

my $pwd = getcwd;
if ($0 =~ /^(.*)\/[^\/]+$/) {
    chdir($pwd = $1);
    $module_name = basename($pwd);
}
require '../virtualmin-tikimanager/virtualmin-tikimanager-lib.pl';

my $d;
my @OLDARGV = @ARGV;

sub usage
{
    print STDERR "$_[0]\n\n" if ($_[0]);
    print "Install Tiki in a virtualmin domain.\n";
    print "\n";
    print "virtualmin tiki-install --domain name | --branch branch\n";
    print "                              [--admin_email]\n";
    print "                              [--sender_email]\n";
    exit(1);
}

sub exit_error
{
    print STDERR "$_[0]\n";
    exit(1);
}

while(@ARGV > 0) {
    my $arg = shift(@ARGV);
    if ($arg eq "--admin_email") {
        $in{'admin_email'} = shift(@ARGV);
    }
    elsif ($arg eq "--sender_email") {
        $in{'sender_email'} = shift(@ARGV);
    }
    elsif ($arg eq "--domain") {
        $in{'domain'} = shift(@ARGV);
    }
    elsif ($arg eq "--branch") {
        $in{'branch'} = shift(@ARGV);
    }
    elsif ($arg eq "--password") {
        $in{'password'} = shift(@ARGV);
    }
    elsif ($arg eq "--help") {
        usage;
    }
}

$d = &virtual_server::get_domain_by("dom", $in{'domain'});
$d || exit_error("The domain '$in{'domain'}' does not exist");

for my $param ('admin_email', 'sender_email') {
    if ($in{$param}) {
        $in{$param} = Email::Valid->address($in{$param});
        $in{$param} || exit_error("$param must be a valid email");
    }
    else {
        $in{$param} = $d->{'email'};
    }
}

$in{'branch'} || exit_error("Branch can't be empty");
my %branches = map { $_ => 1 } &tikimanager_get_installable_branches($d);
if (! exists($branches{$in{'branch'}})) {
    exit_error("$in{'branch'} is not installable.");
}

if ($in{'password'}) {
    tikimanager_validate_password($in{'password'}) || exit_error("Password needs to be at least 8 chars long");
}
else {
    $in{'password'} ||= &tikimanager_generate_password();
}

&tikimanager_tiki_install_and_config($d, \%in, sub { print @_, "\n" });