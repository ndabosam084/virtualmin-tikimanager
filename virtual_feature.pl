use strict;
use warnings;
our (%text);
our $module_name;
our $module_config_directory;

do 'virtualmin-tikimanager-lib.pl';
my $input_name = $module_name;
$input_name =~ s/[^A-Za-z0-9]/_/g;

sub feature_name
{
  return $text{'feat_name'};
}

sub feature_label
{
  return $text{'feat_label'};
}

sub feature_disname
{
  return $text{'feat_disname'};
}

sub feature_hlink
{
  return "label";
}

sub feature_check
{
  return &tikimanager_tiki_is_installable;
}

sub feature_suitable
{
  my ($parent, $alias, $super) = @_;
  return $alias ? 0 : 1;
}

sub feature_enable
{
  my ($d) = @_;
  &$virtual_server::first_print($text{'feat_enable'});
  &$virtual_server::second_print(".. done");
}

sub feature_losing
{
  return $text{'feat_disable'};
}

sub feature_depends
{
  my ($d) = @_;
  return tikimanager_virtualmin_deps($d);
}

sub feature_setup
{
  my ($d) = @_;
  my $success = 1;
  my $extramsg = "";
  &$virtual_server::first_print("Activating Tiki Manager..");

  if (&tikimanager_tiki_is_installed($d)) {
    my ($out, $status) = &tikimanager_tiki_import($d);
    $extramsg = '<br/><pre style="white-space: pre-wrap; background-color: black; color: white;">'.$out.'</pre>';
    $success = $status == "0";
  }

  &$virtual_server::second_print(($success ? ".. done" : ".. failed!") . $extramsg);
  return $success;
}

sub feature_delete
{
  my ($d) = @_;
  &$virtual_server::first_print("Removing $d->{'dom'} from Tiki Manager..");
  &tikimanager_tiki_delete($d);
  &$virtual_server::second_print(".. done");
}

sub feature_validate
{
  my ($d) = @_;
  if (! -r "/opt/tiki-manager/app") {
    return "Missing configuration file";
  }
  else {
    return undef;
  }
}

sub feature_links
{
my ($d) = @_;
  return ({
    'mod' => $module_name,
    'desc' => "Tiki Manager",
    'page' => 'index.cgi?dom=' . $d->{'id'},
    'cat' => 'services',
  });
}

sub feature_webmin
{
  my ($d, $all) = @_;
  my @fdoms = grep { $_->{$module_name} } @$all;

  if (@fdoms) {
    return ( [ $module_name, { 'doms' => join(" ", @fdoms) } ] );
  }
  else {
    return ( );
  }
}